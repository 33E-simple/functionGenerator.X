/*! \file  shortenDate.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 8:06 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include <string.h>

const char szMonths[12][4] = {
  "Jan","Feb","Mar","Apr","May","Jun",
  "Jul","Aug","Sep","Oct","Nov","Dec"
};


/*! shortenDate - Shorten date for display */

/*! Take a date in the form mmm dd yyyy and convert it to
 *  yy-mm-dd to save space on a display.
 *
 */
void shortenDate( char *szDate )
{
  int i,j;
  char szWork[32];
  
  szWork[0]=szDate[9];
  szWork[1]=szDate[10];
  szWork[2]='-';
  j = 99;
  for ( i=0; i<12; i++ )
    if ( !strncmp(szDate,szMonths[i],3) )
      j = i+1;
  sprintf(&szWork[3],"%02d",j);
  szWork[5]='-';
  szWork[6]=szDate[4];
  szWork[7]=szDate[5];
  if ( szWork[6]==' ' )
    szWork[6]='0';
  szWork[8]='\0';
  strcpy(szDate,szWork);
  
}
