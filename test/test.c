#include <stdio.h>
#include <tgmath.h>

#define PI 3.14159268

int main(void)
{
  int i;
  double theta;
  double result;
  
  // Fill sin column
  for ( i=0; i<256; i++ )
    {
      theta = 2.0*PI*(double)i/(double)256;
      result = 126.0 * sin(theta) + 128.0;
      printf("%3d %7.3f  %7.3f \n",i,theta,result);
    }
  return 0;
}



