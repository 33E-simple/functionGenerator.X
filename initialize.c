/*! \file  initialize.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 3:01 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "functionGenerator.h"

/*! initialize() - Initialize clock, ports, timers, output compare  */

/*!
 *
 */
void initialize( void )
{
  int i;
  
  // Set clock to 70 MIPS
  CLKDIVbits.FRCDIV = 0;    // Divide by 1 = 8MHz (default /2)
  CLKDIVbits.PLLPRE = 0;    // Divide by 2 = 4 MHz (default)
  PLLFBD = DEFAULT_SPEED;   // Multiply by 70 = 280 (default *50)
  CLKDIVbits.PLLPOST = 0;   // Divide by 2 = 140 = 70 MIPS (def. 50/25)
  
  _ANSB8 = 0;
  _ANSB9 = 0;
  _ANSA4 = 0;
  
  _TRISB6 = 0;
  _TRISB7 = 0;
  _TRISB4 = 0;
  
  LCDinit();
  
  for ( i=0; i<4; i++ )
    butstates[i] = 1;
  menulevel = 0;
  tablePointer = 0;
  
  makeWaveTable();
  
  // PWM update timing
  PR3 = 52640;  // Default 5 hZ
  T3CONbits.TCKPS = 0;
  T3CONbits.TON = 1;
  _T3IE = 1;

  // PWM setup
  PR5 = 256;
  T5CONbits.TCKPS = 0;
  //OC2TMR = 256;
  OC2R = waveTable[waveform][tablePointer];
  //OC2CON1bits.TRIGMODE = 1;
  OC2CON1bits.OCTSEL = 3; // T5 clock
  OC2CON1bits.OCM = 5; // Edge-aligned PWM
  OC2CON2bits.SYNCSEL = 15; // Sync on timer 5
  OC2RS = 1; // 

  // See simplTest.h for OC mapping
  RB7PPS = MAPOC2;
  // Start the timer
  T5CONbits.TON = 1;

 
  
}
