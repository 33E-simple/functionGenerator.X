/*! \file  T3interrupt.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 4:44 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "functionGenerator.h"

/*! T3interrupt - Update PWM duty cycle */

/*!
 *
 */
void __attribute__((__interrupt__, auto_psv)) _T3Interrupt( void)
{
  _T3IF = 0; // Clear the interrupt flag

  OC2RS = waveTable[waveform][tablePointer];

  tablePointer++;
  if ( tablePointer>255 )
    tablePointer = 0;
}
