/*! \file  setWaveUpdate.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 4:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "functionGenerator.h"

unsigned int PR3values[8] = 
{
  52640,        // 5 Hz
  26320,        // 10 Hz
  13160,        // 20 Hz
  5264,         // 50 Hz
  2632,         // 100 Hz
  1316,         // 200 Hz
  526,          // 500 Hz
  263           // 1 kHz
};

/*! setWaveUpdate - */

/*!
 *
 */
void setWaveUpdate(void)
{
  PR3 = PR3values[frequency];
}
