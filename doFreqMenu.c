/*! \file  doFreqMenu.c
 *
 *  \brief Select from a list of frequencies
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 3:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "functionGenerator.h"

/*! List of frequencies for display */
const char menu3[10][18] = {
  "5 Hz            ",
  "10 Hz           ",
  "20 Hz           ",
  "50 Hz           ",
  "100 Hz          ",
  "200 Hz          ",
  "500 Hz          ",
  "1 kHz           "
};

/*! doFreqMenu - Select frequency menu */

/*!
 *
 */
void doFreqMenu( void )
{
  int nextline;
  int dirty;
  int state;
  
  /* Initially set display to be needing update */
  dirty = 1;
  
  /* Button 2 (right) means keep this current result */
  while ( !butstates[2] )
    {
      checkButStates();
      state=butstates[0];
      if ( state )
      {
          frequency--;
          if ( frequency<0 )
            frequency = FREQCHOICES-1;
          dirty = 1;
          butstates[0] = 0;
      }
      state=butstates[3];
      if ( state )
      {
          frequency++;
          if ( frequency>=FREQCHOICES )
            frequency = 0;
          dirty = 1;
          butstates[3] = 0;
      }
      if ( dirty)
        {
          LCDposition(0);
          LCDputs((char *) menu3[frequency]);
          nextline = frequency + 1;
          if (nextline >= FREQCHOICES)
            nextline = 0;
          LCDposition(0x40);
          LCDputs((char *) menu3[nextline]);
          LCDposition(15);
          LCDputs(">");
          dirty = 0;
          Delay_ms(BUTTONWAIT);
        }
    }
  butstates[2] = 0;  
}
