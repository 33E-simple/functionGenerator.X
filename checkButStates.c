/*! \file  checkButStates.c
 *
 *  \brief Check button states
 *
 *
 *  \author jjmcd
 *  \date August 30, 2015, 9:23 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/LCD.h"  /* For Delay_ms() */
#include "functionGenerator.h"


/*! checkButStates - Keep track of the states of the buttons */

/*! checkButtonStates() remembers an array of the current state of
 *  each button as a toggle.  If a button is released, the remembered
 *  state is switched.  A function may then come and "process" the
 *  button press and then set it back, if desired.  When used this
 *  way, the button array butstates[] remembers any button press
 *  that has not yet been handled.
 */
void checkButStates( void )
{
  /* Up button */
  /* If it's up and didn't used to be up */
  if ( SW1 && !oldstates[0] )
    {
      /* Remember that it is now down */
      oldstates[0]  = 1;
      /* And change the remembered state */
      butstates[0] ^= 1;
    }

  /* Left button */
  if ( SW2 && !oldstates[1] )
    {
      oldstates[1]  = 1;
      butstates[1] ^= 1;
    }

  /* Right button */
  if ( SW3 && !oldstates[2] )
    {
      oldstates[2]  = 1;
      butstates[2] ^= 1;
    }

  /* Down button */
  if ( SW4 && !oldstates[3] )
    {
      oldstates[3]  = 1;
      butstates[3] ^= 1;
    }

  /* Now set each of the old states to their current state */
  oldstates[0] = SW1;
  oldstates[1] = SW2;
  oldstates[2] = SW3;
  oldstates[3] = SW4;
  /* Give it a millisecond in case of bounce */
  Delay_ms(1);
}
