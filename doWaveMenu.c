/*! \file  doWaveMenu.c
 *
 *  \brief Select from a list of waveforms
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 3:06 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "functionGenerator.h"


const char menu2[4][18] = {
  "Sine            ",
  "Square          ",
  "Sawtooth        ",
  "Triangle        "
};

/*! doWaveMenu - Waveform menu */

/*!
 *
 */
void doWaveMenu( void )
{
  int nextline;
  int dirty;
  int state;
  
  dirty = 1;
  
  while ( !butstates[2] )
    {
      checkButStates();
      state=butstates[0];
      if ( state )
      {
          waveform--;
          if ( waveform<0 )
            waveform = WAVECHOICES-1;
          dirty = 1;
          butstates[0] = 0;
      }
      state=butstates[3];
      if ( state )
      {
          waveform++;
          if ( waveform>=WAVECHOICES )
            waveform = 0;
          dirty = 1;
          butstates[3] = 0;
      }
      if ( dirty)
        {
          LCDposition(0);
          LCDputs((char *) menu2[waveform]);
          nextline = waveform + 1;
          if (nextline >= WAVECHOICES)
            nextline = 0;
          LCDposition(0x40);
          LCDputs((char *) menu2[nextline]);
          LCDposition(15);
          LCDputs(">");
          dirty = 0;
          Delay_ms(BUTTONWAIT);
        }
    }
  butstates[2] = 0;  
}
