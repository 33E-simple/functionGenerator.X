/*! \file  functionGenerator.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 5, 2015, 6:41 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef FUNCTIONGENERATOR__H
#define	FUNCTIONGENERATOR__H

#ifdef	__cplusplus
extern "C"
{
#endif
#include <xc.h>
#include "../include/dsPIC-EL-GM.h"

#ifndef EXTERN
#define EXTERN extern
#endif

  /*! PLL multiplier */
#define DEFAULT_SPEED 72

/*! Number of choices on the top menu */
#define TOPCHOICES 2
/*! Number of choices on the waveform menu */
#define WAVECHOICES 4
/*! Number of choices on the frequency menu */
#define FREQCHOICES 8

#define PREV 0
#define NEXT 3
#define SELECT 2
#define BACK 1
  
/*! Time to delay after processing buttons for debounce */
#define BUTTONWAIT 1

/*! Register for mapping RB6 pin for output */
#define RB6PPS RPOR2bits.RP38R
/*! Register for mapping RB7 pin for output */
#define RB7PPS RPOR2bits.RP39R
/*! Value for pin select for output compare 1 */
#define MAPOC1 16
/*! Value for pin select for output compare 2 */
#define MAPOC2 17
/*! Value for pin select for output compare 3 */
#define MAPOC3 18
/*! Value for pin select for output compare 4 */
#define MAPOC4 19

/*! State of each button for processing */
EXTERN int butstates[4];
/*! Previous button states */
EXTERN int oldstates[4];
/*! Table of PWM values */
EXTERN unsigned int waveTable[4][256];

/*! Current choice on top menu */
EXTERN int topchoice;
/*! Current choice on waveform menu */
EXTERN int waveform;
/*! Current choice on frequency menu */
EXTERN int frequency;
/*! Depth of menu nesting */
EXTERN int menulevel;
/*! Current entry in waveTable[] being used */
EXTERN int tablePointer;

void checkButStates( void );
/*! initialize() - Initialize clock, ports, timers, output compare  */
void initialize( void );
void doTopMenu( void );
void doWaveMenu( void );
void doFreqMenu( void );
/*! makeWaveTable() - make a table of waveforms */
void makeWaveTable( void );
void setWaveUpdate( void );
void announce( char *, char * );
void shortenDate( char * );


#ifdef	__cplusplus
}
#endif

#endif	/* FUNCTIONGENERATOR__H */

