/*! \file  doTopMenu.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 3:04 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "functionGenerator.h"

const char menu1[2][18] = {
  "Waveform        ",
  "Frequency       "
};

/*! doTopMenu - Top level menu  */

/*!
 *
 */
void doTopMenu( void )
{
  int nextline;
  int dirty;
  int state;
  
  dirty = 1;
  
  while ( !butstates[2] )
    {
      checkButStates();
      state=butstates[0];
      if ( state )
      {
          topchoice--;
          if ( topchoice<0 )
            topchoice = TOPCHOICES-1;
          dirty = 1;
          butstates[0] = 0;
      }
      state=butstates[3];
      if ( state )
      {
          topchoice++;
          if ( topchoice>=TOPCHOICES )
            topchoice = 0;
          dirty = 1;
          butstates[3] = 0;
      }
      if ( dirty)
        {
          LCDposition(0);
          LCDputs((char *) menu1[topchoice]);
          nextline = topchoice + 1;
          if (nextline >= TOPCHOICES)
            nextline = 0;
          LCDposition(0x40);
          LCDputs((char *) menu1[nextline]);
          LCDposition(15);
          LCDputs(">");
          dirty = 0;
          Delay_ms(BUTTONWAIT);
        }
    }
  butstates[2] = 0;
}
