## Function Generator

A simple, low frequency waveform generator for the dsPIC-EL-GM board.

Mostly an exercise in experimenting with nested menus.

Application developed on a dsPIC33EV256GM102, but any of the following
PICs might be used:

* dsPIC33EV64GM002
* dsPIC33EV64GM102
* dsPIC33EV128GM002
* dsPIC33EV128GM102
* dsPIC33EV256GM002
* dsPIC33EV256GM102

---

![Scope trace](https://gitlab.com/33E-simple/functionGenerator.X/raw/master/images/IMAG042-0-40000.jpg)

---

