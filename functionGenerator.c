/*! \file  functionGenerator.c
 *
 *  \brief Simple low-frequency waveform generator
 *
 *
 *  \author jjmcd
 *  \date September 5, 2015, 6:40 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <string.h>
#include <stdio.h>
#include "../include/LCD.h"
#define EXTERN
#include "functionGenerator.h"
#include "configBits.h"

/*! main - Mainline for the waveform generator */
/*!
 *
 */
int main(void)
{
  initialize();
  announce(__DATE__,__TIME__);

  while (1)
    {
      checkButStates();
      if (!butstates[1])
        {
          switch (menulevel)
            {
              case 0:
                doTopMenu();
                if (topchoice)
                  menulevel = 2;
                else
                  menulevel = 1;
                break;
              case 1:
                doWaveMenu();
                menulevel = 0;
                break;
              case 2:
                doFreqMenu();
                setWaveUpdate();
                menulevel = 0;
                break;
              default:
                menulevel = 0;
                break;
            }
        }
    }
return 0;
}
