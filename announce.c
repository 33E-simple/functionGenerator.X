/*! \file  announce.c
 *
 *  \brief Announce the app with compile date and time
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 8:02 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "../include/LCD.h"
#include "functionGenerator.h"


/*! announce - Display the app and compile date and time */

/*! Shortens the date passed in (from __DATE__) and displays the
 *  date and time on the right and name on the left.  Delays
 *  2 seconds before clearing the LCD.
 */
void announce( char *szDatei, char *szTime )
{
  char szDate[16];
  
  LCDclear();
  LCDputs("Wavefrm");
  LCDline2();
  LCDputs("Genrtor");
  LCDposition(8);
  strcpy(szDate,szDatei);
  shortenDate(szDate);
  LCDputs(szDate);
  LCDposition(0x48);
  LCDputs(szTime);
  Delay_ms(3000);
  LCDclear(); 
}
