/*! \file  makeWaveTable.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 6, 2015, 4:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <math.h>
#include <dsp.h>
#include "functionGenerator.h"


/*! makeWaveTable() - make a table of waveforms */

/*!
 *
 */
void makeWaveTable(void)
{
  int i;
  double theta;
  double result;
  
  // Fill sin column
  for ( i=0; i<256; i++ )
    {
      theta = 2.0*PI*(double)i/(double)256;
      result = 60.0 * sin(theta) + 128.0;
      waveTable[0][i] = (unsigned int) ceil( result );
    }
  
  // Fill square column
  for ( i=0; i<128; i++ )
    waveTable[1][i] = 1;
  for ( i=128; i<256; i++ )
    waveTable[1][i] = 254;
  
  // Fill sawtooth column
  for ( i=0; i<256; i++ )
    waveTable[2][i] = i;
  
  // Fill triangle column
  for ( i=0; i<128; i++ )
    waveTable[3][i] = (unsigned int)((double)i*0.9);
  for ( i=128; i<256; i++ )
    waveTable[3][i] = (unsigned int)((double)(256-i)*0.9);
}
